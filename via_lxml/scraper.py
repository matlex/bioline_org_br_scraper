# *-* coding: utf-8
"""
bioline.org.br publications scraper
"""

import datetime
import logging
from urlparse import urljoin
from grab import Grab
import config
from lxml.html import fromstring
from mongoengine import connect, Document, DoesNotExist, StringField

# Initial constants
DOMAIN = "http://bioline.org.br/"
STARTING_URL = "http://bioline.org.br/titles?id=md&year=2016&vol=19&num=01&keys=V19N1"
SOME_ARTICLE_URL = "http://bioline.org.br/abstract?id=md16002&lang=en"
g = Grab()

# Setup logging handler
logging.basicConfig(
    filename='scraper.log',
    format='%(asctime)s: %(message)s',
    level=logging.INFO
)


# Define MongoDB documents schema
class Publication(Document):
    article_url = StringField(required=True, max_length=250)
    journal_name = StringField(max_length=250)
    journal_issn = StringField(max_length=30)
    title = StringField(max_length=250)
    authors = StringField(max_length=250)
    full_text = StringField()
    author_affiliations = StringField()
    abstract = StringField()


def get_html(url):
    """
    Takes target url and returns it's html source
    :param url:
    :return:
    """
    response = g.go(url)
    return response.body


def get_articles_list_urls(url):
    """
    Takes given url, visits it and scrapes webpage.
    Return list with articles urls.
    :param url:
    :return: [article_url1, article_urlNth]
    """
    html_source = get_html(url)
    parsed_html = fromstring(html_source)
    output_urls = []

    articles_li_tags = parsed_html.xpath("*//ul//li")
    for li_tag in articles_li_tags[1:]:  # First li is a table header, so pass it.
        try:
            article_url = li_tag.xpath('a')[0].xpath('@href')[0]
            output_urls.append(urljoin(DOMAIN, article_url))
        except IndexError:
            pass
    return output_urls


def get_data_from_article_page(article_url):
    """
    Extracts required publication data from a given article url.
    :param article_url:
    :return:
    """
    html_source = get_html(article_url)
    parsed_html = fromstring(html_source)

    journal_name = parsed_html.xpath('//font[@class="paperTitle"]/text()')[0]
    journal_issn = parsed_html.xpath('//font[@class="paperISSN"]/text()')[0]
    title = parsed_html.xpath('//font[@class="AbstractTitle"]/text()')[0]
    authors = parsed_html.xpath('//font[@class="AbstractAuthor"]/text()')[0]
    full_text = parsed_html.xpath('//div[@class="AbstractText"]/text()')[0]
    author_affiliations = None
    abstract = None

    output_dict = {
        "article_url": article_url,
        "journal_name": journal_name,
        "journal_issn": journal_issn,
        "title": title,
        "authors": authors,
        "full_text": full_text,
        "author_affiliations":author_affiliations,
        "abstract": abstract
    }
    return output_dict


def store_article_data_in_db(article_data):
    """
    Takes a dictionary with article data, checks is there that publication in DB,
    and save in DB if it doesn't exist.
    :param article_data:
    :return:
    """
    try:
        # If document with the given article url exists, then do nothing and just pass it.
        Publication.objects.get(article_url=article_data["article_url"])
        print "Publication already exists. {}".format(article_data["article_url"])
        logging.info("Publication already exists. {}".format(article_data["article_url"]))
    except DoesNotExist:
        publication = Publication(
            article_url=article_data["article_url"],
            journal_name=article_data["journal_name"],
            journal_issn=article_data["journal_issn"],
            title=article_data["title"],
            authors=article_data["authors"],
            full_text=article_data["full_text"],
            author_affiliations=article_data["author_affiliations"],
            abstract=article_data["abstract"]
        )
        publication.save()
        print "Publication successfully saved to MongoDB. {}".format(article_data["article_url"])
        logging.info("Publication successfully saved to MongoDB. {}".format(article_data["article_url"]))


def main():
    """
    The main scraping process executes here.
    :return:
    """
    articles_urls = get_articles_list_urls(STARTING_URL)
    connect('{0}', host='mongodb://{1}:{2}@ds053176.mlab.com:53176/{3}'.format(
        config.DB_NAME, config.DB_USER, config.DB_USER_PASSWORD, config.DB_NAME))
    for article_url in articles_urls[:]:
        article_data = get_data_from_article_page(article_url)
        store_article_data_in_db(article_data)
    print "[SUCCESS]: Scraping process successfully executed."
    logging.info("[SUCCESS]: Scraping process successfully executed.")


if __name__ == '__main__':
    logging.info("Starting new scraping process.")
    start_time = datetime.datetime.now()
    try:
        main()
    except Exception as e:
        logging.info("[ERROR]: \n{}\n".format(e))
    end_time = datetime.datetime.now()
    print "Started at: {0} Ended at: {1}".format(start_time.strftime("%Y:%m:%d %H:%M:%S"),
                                                 end_time.strftime("%Y:%m:%d %H:%M:%S"))
    print "Execution time: {}".format(end_time - start_time)
    logging.info("Started at: {0} Ended at: {1}".format(start_time.strftime("%Y:%m:%d %H:%M:%S"),
                                                        end_time.strftime("%Y:%m:%d %H:%M:%S")))
    logging.info("Execution time: {}".format(end_time - start_time))
    logging.info("=" * 100)
