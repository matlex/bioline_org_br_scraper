# bioline.org.br publications scraper project #
## What is this? ##
Test task for hiring.
## What it does? ##
Extracts publications data from web site and save it into remote MongoDB database.
## How to run? ##
* Create a clean virtual environment.
* Go to main application's directory and run pip install -r requirements.txt
* Copy config_template.py to config.py and fill in MongoDB credentials in it.
* Launch application by running command "python scraper.py"
* See some execution info in stdout or in scraper.log file

## Prerequisites ##
1. **Python 2.7**
2. **Before installing lxml library install:**

* sudo apt-get install libxml2-dev libxslt-dev python-dev  
* sudo apt-get install python-lxml  

## TO-DOs ##
* Reimplement that scraper with Scrapy Framework